<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;


class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $permissions = Permission::all();
        return view('backend.pages.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.permissions.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ], [
            'name' => 'Permission name is required.',
        ]);


        $permission = new Permission();
        $permission->group_name= strtolower($request->group_name);
        $permission->name= strtolower($request->name);
        $permission->guard_name= 'admin';
        $permission->save();

        session()->flash('success', 'Permission created successfully.');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

            $permission = Permission::findOrfail($id);
            return view('backend.pages.permissions.edit',compact('permission'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
        ], [
            'name' => 'Permission name is required.',
        ]);

        $permission = Permission::findOrfail($id);
        $permission->group_name= strtolower($request->group_name);
        $permission->name= strtolower($request->name);
        $permission->guard_name= 'admin';
        $permission->save();

        session()->flash('success', 'Permission updated successfully.');
        return back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $permission = Permission::findOrfail($id);
        $permission->delete();
        session()->flash('success', 'Permission has been deleted successfully.');
        return back();

    }
}
