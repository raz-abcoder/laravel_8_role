/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : laravel_role

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-09-23 19:37:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admins`
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`),
  UNIQUE KEY `admins_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES ('1', 'Super Admin', 'superadmin@example.com', 'superadmin', null, '$2y$10$04D4VxiS5fbr8ll6towqjeTK21rP8MHxERjgpTeES5rdLD0c9bIsi', null, '2020-09-12 04:33:43', '2020-09-12 04:33:43');
INSERT INTO `admins` VALUES ('2', 'Admin', 'admin@gmail.com', 'admin', null, '$2y$10$.7HeNIXmdognq7i3eQ8YzOg/EXqzrJsSB0BqfXQBa8iB86JClmCs2', null, '2020-09-20 16:35:35', '2020-09-20 16:35:35');
INSERT INTO `admins` VALUES ('3', 'Client', 'client@example.com', 'client', null, '$2y$10$Nf4rahrK4DohTyAPdJWGeuEHddulfF0D3bANvI9mLNT7svLT88Pge', null, '2020-09-20 16:45:00', '2020-09-20 16:45:00');
INSERT INTO `admins` VALUES ('4', 'Test Admin', 'test@gmail.com', 'test', null, '$2y$10$i8mt.IzUxq9mq/3euOJ8E.CF7gprJCVNyV.mR8Vdn2LQgcLo/8kCi', null, '2020-09-25 19:18:32', '2020-09-25 19:18:32');

-- ----------------------------
-- Table structure for `failed_jobs`
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('29', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('30', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('31', '2019_08_19_000000_create_failed_jobs_table', '1');
INSERT INTO `migrations` VALUES ('32', '2020_07_24_184706_create_permission_tables', '1');
INSERT INTO `migrations` VALUES ('33', '2020_09_12_043205_create_admins_table', '2');

-- ----------------------------
-- Table structure for `model_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of model_has_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `model_has_roles`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of model_has_roles
-- ----------------------------
INSERT INTO `model_has_roles` VALUES ('1', 'App\\Models\\Admin', '1');
INSERT INTO `model_has_roles` VALUES ('6', 'App\\Models\\Admin', '2');
INSERT INTO `model_has_roles` VALUES ('7', 'App\\Models\\Admin', '3');
INSERT INTO `model_has_roles` VALUES ('1', 'App\\User', '3');
INSERT INTO `model_has_roles` VALUES ('6', 'App\\User', '3');
INSERT INTO `model_has_roles` VALUES ('8', 'App\\Models\\Admin', '4');

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'dashboard.view', 'admin', 'dashboard', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('2', 'dashboard.edit', 'admin', 'dashboard', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('3', 'blog.create', 'admin', 'blog', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('4', 'blog.view', 'admin', 'blog', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('5', 'blog.edit', 'admin', 'blog', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('6', 'blog.delete', 'admin', 'blog', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('7', 'blog.approve', 'admin', 'blog', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('8', 'admin.create', 'admin', 'admin', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('9', 'admin.view', 'admin', 'admin', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('10', 'admin.edit', 'admin', 'admin', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('11', 'admin.delete', 'admin', 'admin', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('12', 'admin.approve', 'admin', 'admin', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('13', 'role.create', 'admin', 'role', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('14', 'role.view', 'admin', 'role', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('15', 'role.edit', 'admin', 'role', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('16', 'role.delete', 'admin', 'role', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('17', 'role.approve', 'admin', 'role', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('18', 'profile.view', 'admin', 'profile', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('19', 'profile.edit', 'admin', 'profile', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `permissions` VALUES ('20', 'permissions.index', 'admin', 'permissions', '2021-09-23 10:51:48', '2021-09-23 11:02:23');
INSERT INTO `permissions` VALUES ('21', 'permissions.view', 'admin', 'permissions', '2021-09-23 10:59:25', '2021-09-23 11:02:45');
INSERT INTO `permissions` VALUES ('22', 'permissions.create', 'admin', 'permissions', '2021-09-23 10:59:42', '2021-09-23 11:02:58');
INSERT INTO `permissions` VALUES ('24', 'dd.fff', 'admin', '', '2021-09-23 12:41:35', '2021-09-23 12:41:35');
INSERT INTO `permissions` VALUES ('25', 'users.index', 'admin', 'users', '2021-09-23 13:25:03', '2021-09-23 13:25:03');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'superadmin', 'admin', '2020-07-25 16:43:33', '2020-07-25 16:43:33');
INSERT INTO `roles` VALUES ('6', 'Admin', 'admin', '2020-08-13 15:44:25', '2020-08-14 08:10:53');
INSERT INTO `roles` VALUES ('7', 'Client', 'admin', '2020-09-20 16:44:27', '2020-09-20 16:44:27');
INSERT INTO `roles` VALUES ('8', 'Test Role', 'admin', '2020-09-25 19:17:53', '2020-09-25 19:17:53');

-- ----------------------------
-- Table structure for `role_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------
INSERT INTO `role_has_permissions` VALUES ('1', '1');
INSERT INTO `role_has_permissions` VALUES ('2', '1');
INSERT INTO `role_has_permissions` VALUES ('3', '1');
INSERT INTO `role_has_permissions` VALUES ('4', '1');
INSERT INTO `role_has_permissions` VALUES ('5', '1');
INSERT INTO `role_has_permissions` VALUES ('6', '1');
INSERT INTO `role_has_permissions` VALUES ('7', '1');
INSERT INTO `role_has_permissions` VALUES ('8', '1');
INSERT INTO `role_has_permissions` VALUES ('9', '1');
INSERT INTO `role_has_permissions` VALUES ('10', '1');
INSERT INTO `role_has_permissions` VALUES ('11', '1');
INSERT INTO `role_has_permissions` VALUES ('12', '1');
INSERT INTO `role_has_permissions` VALUES ('13', '1');
INSERT INTO `role_has_permissions` VALUES ('14', '1');
INSERT INTO `role_has_permissions` VALUES ('15', '1');
INSERT INTO `role_has_permissions` VALUES ('16', '1');
INSERT INTO `role_has_permissions` VALUES ('17', '1');
INSERT INTO `role_has_permissions` VALUES ('18', '1');
INSERT INTO `role_has_permissions` VALUES ('19', '1');
INSERT INTO `role_has_permissions` VALUES ('20', '1');
INSERT INTO `role_has_permissions` VALUES ('21', '1');
INSERT INTO `role_has_permissions` VALUES ('22', '1');
INSERT INTO `role_has_permissions` VALUES ('1', '6');
INSERT INTO `role_has_permissions` VALUES ('2', '6');
INSERT INTO `role_has_permissions` VALUES ('3', '6');
INSERT INTO `role_has_permissions` VALUES ('4', '6');
INSERT INTO `role_has_permissions` VALUES ('5', '6');
INSERT INTO `role_has_permissions` VALUES ('6', '6');
INSERT INTO `role_has_permissions` VALUES ('7', '6');
INSERT INTO `role_has_permissions` VALUES ('9', '6');
INSERT INTO `role_has_permissions` VALUES ('13', '6');
INSERT INTO `role_has_permissions` VALUES ('14', '6');
INSERT INTO `role_has_permissions` VALUES ('1', '7');
INSERT INTO `role_has_permissions` VALUES ('4', '7');
INSERT INTO `role_has_permissions` VALUES ('14', '7');
INSERT INTO `role_has_permissions` VALUES ('1', '8');
INSERT INTO `role_has_permissions` VALUES ('2', '8');
INSERT INTO `role_has_permissions` VALUES ('8', '8');
INSERT INTO `role_has_permissions` VALUES ('9', '8');
INSERT INTO `role_has_permissions` VALUES ('10', '8');
INSERT INTO `role_has_permissions` VALUES ('11', '8');
INSERT INTO `role_has_permissions` VALUES ('12', '8');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Karim Ali', 'karim@gmail.com', null, '$2y$10$vOFw0OHBxic5MvQRjYFBkuWifJkEHJBF1CEglOSgRnymXwMyLuunK', null, '2020-07-25 16:43:33', '2021-09-23 13:23:34');
INSERT INTO `users` VALUES ('3', 'test2', 'admin@akijfood.com', null, '$2y$10$/4YK7i2fVqdTfm0WAJM3lOnApbJx4w9vxUBZyjN3HqBTnV0zmcwO.', null, '2020-08-14 08:28:22', '2020-08-14 09:07:31');
