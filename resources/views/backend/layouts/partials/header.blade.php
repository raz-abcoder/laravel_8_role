<!-- header area start -->
<div class="header-area">
    <div class="row align-items-center">
        <!-- nav and search button -->
        <div class="col-md-6 col-sm-8 clearfix">
        </div>
        <!-- profile info & task notification -->
        <div class="col-md-6 col-sm-4 clearfix">
            <ul class="notification-area pull-right">

                <li class="dropdown">
                    <i class="ti-user dropdown-toggle" data-toggle="dropdown"></i>
                    <div class="dropdown-menu ">
                        <a class="dropdown-item" href="{{ route('admin.logout.submit') }}"
                           onclick="event.preventDefault();
                      document.getElementById('admin-logout-form').submit();">Log Out</a>
                        <form id="admin-logout-form" action="{{ route('admin.logout.submit') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>



            </ul>
        </div>
    </div>
</div>
<!-- header area end -->