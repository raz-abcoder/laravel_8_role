# Laravel Role Permission Management System - Laravel 8.x

This is a basic Laravel Project with Laravel Role Permission Management System. 

## Project Setup 
1. Git clone - git clone https://raz-abcoder@bitbucket.org/raz-abcoder/laravel_8_role.git
1. Install Laravel Dependencies - `composer update`
1. Create database - `laravel_role`
Since, there is any problem to seeder, Please import the .sql file directly - https://bitbucket.org/raz-abcoder/laravel_8_role/src/master/database/sql/laravel_role.sql
1. Run Project - `php artisan serve`


So, You've got the project of Laravel Role & Permission Management on your http://localhost:8000

## Setup 2
1. Login using Super Admin Credential - 
    1. Username - superadmin
    1. Password - 12345678
2. Create Role
3. Create Admin
4. Create Permission
5. Assign Permission to Roles
6. Assign Multiple Role to an admin




### Dashboard Page
![alt text][dashboardImage]

### Role Pages
Role List
![alt text][roleListImage]
Role Create
![alt text][roleCreateImage]
Role Edit
![alt text][roleEditImage]

### Admin Pages
Admin List
![alt text][adminListImage]
Admin Create
![alt text][adminCreateImage]

### Other Pages
Login Page
![alt text][adminLoginImage]
Custom Error Pages
![alt text][errorPageImage]
Dynamic Sidebar Manage
![alt text][sidebarDyanamic]


### For conact raz.cse08@gmail.com | +8801724160299

